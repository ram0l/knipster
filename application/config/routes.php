<?php

$router = $di->get('router');
$router->setDefaultModule('Api');
$modules = $application->getModules();
foreach ($application->getModules() as $key => $module) {
    include_once realpath(dirname($module['path']) . '/config/routes.php');
}
$di->set('router', $router);
