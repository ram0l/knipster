<?php

return new \Phalcon\Config([
    'database' => [
        'adapter'  => 'Mysql',
        'host'     => 'knipster_mysql_1',
        'username' => 'phalcon',
        'password' => 'secret',
        'dbname'   => 'phalcondb',
        'charset'  => 'utf8',
    ],
    'redis' => [
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'port' => 6379,
        'persistent' => true
    ]
]);
