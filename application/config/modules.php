<?php

/**
 * Register application modules
 */
$application->registerModules(array(
    'Api' => array(
        'className' => 'Knipster\Api\Module',
        'path' => __DIR__ . '/../apps/api/Module.php'
    )
));
