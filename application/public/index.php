<?php

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

define('APP_PATH', realpath('..'));
include APP_PATH . '/functions.php';

use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;

if (getEnvironment() == ENV_PROD) {
    error_reporting(0);
}

try {
    /**
     * loader
     */
    include APP_PATH . '/config/loader.php';

    $di = new Phalcon\Di\FactoryDefault();

    $Bootstrap = new \Lib\Core\Bootstrap($di);
    $Bootstrap->loadConfig();
    $Bootstrap->loadServices();
    $Bootstrap->checkEnvironment();

    /**
     * Handle the request
     */
    $application = new Phalcon\Mvc\Application($di);

    /**
     * Include modules
     */
    require __DIR__ . '/../config/modules.php';

    /**
     * Include routes
     */
    require __DIR__ . '/../config/routes.php';

    echo $application->handle()->getContent();

    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $finish = $time;
    $total_time = round(($finish - $start), 4);

    if (isset($_GET['_time']) && isDev()) {
        echo "\n\nGenerated in: " . $total_time . ' seconds';
    }
} catch (\Exception $e) {
    if (!isProd()) {
        p('wtf?!');
        p($e->getMessage());
        pp($e->getTraceAsString());
    } else {
        pp('500');
    }
    exit;
}
