#!/usr/bin/env bash

function build()
{
    case "$1" in
            dev)
                echo '###########################################'
                echo '               DEVELOPMENT'
                echo '###########################################'

                check_environment;
                make_composer;
                #make_bower;
                make_clean_up;
                echo '-- build.sh done'
                ;;

            staging)
                echo '###########################################'
                echo '                 STAGING'
                echo '###########################################'

                check_environment;
                #make_composer;
                #make_bower;
                make_clean_up;

                echo '-- build.sh done'
                ;;

            production)
                echo '###########################################'
                echo '               PRODUCTION'
                echo '###########################################'

                check_environment;
                #make_composer;
                #make_bower;
                make_clean_up;
                echo '-- build.sh done'
                ;;
            *)
                echo $"Usage: $0 {dev|staging|production}"
                exit 1
    esac
}

######
###### helpers ..
######

function check_environment()
{
    # check if php is installed
    if ! php -v >/dev/null 2>&1;
        then
            echo 'FAIL: php is required';
            exit 1;
    fi

    # cache dir
    if [ ! -d cache ]; then
        echo '-- creating cache dir'
        mkdir cache;
        chmod -R 0755 cache;
    fi;
}

function make_bower() {
    bower update;
}

function make_clean_up()
{
    rm -rf ./cache/*
}

make_composer()
{
    # update composer and then run robo file
    if composer >/dev/null 2>&1;
        then
            composer update
    else
        echo 'FAIL: composer is not installed globally';
        exit 1;
    fi
}

# execute build script
build $1;