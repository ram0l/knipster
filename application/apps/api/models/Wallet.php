<?php

namespace Knipster\Api\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;

/**
 * Class Wallet
 *
 * @package Knipster\Api\Models
 */
class Wallet extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $total_balance;

    /**
     * @var int
     */
    public $bonus_balance;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'wallet';
    }

    /**
     * @param array $parameters
     *
     * @return Wallet[]
     */
    public static function find($parameters = [])
    {
        return parent::find($parameters);
    }

    /**
     * @param null $parameters
     *
     * @return Wallet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Return user's wallet (create one if needed)
     *
     * @param int $userId
     *
     * @return \Knipster\Api\Models\Wallet
     */
    public static function factory(int $userId)
    {
        $wallet = self::findFirst([
            'conditions' => 'user_id=:user_id:',
            'bind' => ['user_id' => $userId],
            'for_update' => true
        ]);

        if (!$wallet) {
            $user = User::findFirst($userId);
            if (!$user) {
                return null;
            }

            $wallet = new Wallet();
            $wallet->user_id = $userId;
            if (!$wallet->create()) {
                return null;
            }
        }

        $tmp = new self();
        $tmp->assign($wallet->toArray());

        return $tmp;
    }

    /**
     * @param float $amount
     *
     * @return bool
     */
    public function deposit(float $amount): bool
    {
        if ($amount < 0) {
            $this->appendMessage(new Model\Message('Amount is below 1'));
            return false;
        }

        $walletHistory = new WalletHistory();
        $walletHistory->amount = $amount;
        $walletHistory->operation = WalletHistory::OPERATION_DEPOSIT;
        $walletHistory->user_id = $this->user_id;
        $walletHistory->wallet_id = $this->id;

        $user = User::findFirst($this->user_id);

        // calculate bonus
        $bonusAmount = $amount * $user->bonus * 0.01;
        $amount += $bonusAmount;

        $this->total_balance += $amount;
        $this->bonus_balance += $bonusAmount;
        if (!$this->update()) {
            return false;
        }

        return $walletHistory->create();
    }

    /**
     * @param float $amount
     *
     * @return bool
     */
    public function withdraw(float $amount): bool
    {
        if ($amount < 0) {
            $this->appendMessage(new Model\Message('Amount is below 1'));
            return false;
        }

        $walletHistory = new WalletHistory();
        $walletHistory->amount = $amount;
        $walletHistory->operation = WalletHistory::OPERATION_WITHDRAW;
        $walletHistory->user_id = $this->user_id;
        $walletHistory->wallet_id = $this->id;

        // can't withdraw more than total_balance-bonus_balance
        if ($this->total_balance - $this->bonus_balance < $amount) {
            $this->appendMessage(new Model\Message('Balance is too low'));
            return false;
        }

        $this->total_balance -= $amount;
        if (!$this->update()) {
            return false;
        }

        return $walletHistory->create();
    }

    /**
     * @param int $days
     *
     * @return array
     */
    public function getReport(int $days = 7): array
    {
        $sql = <<<EOT
        select h.created,u.country,count(distinct h.user_id) as unique_users,
            count(*) as total_operations, 
            sum(IF(h.`operation`=1,h.`amount`,0)) as total_deposit, 
            sum(IF(h.`operation`=2,h.`amount`,0)) as total_withdraw
	        from wallet_history h 
	        left join user u 
		        on h.user_id=u.id
            where h.`created` > DATE(NOW()) - INTERVAL ? DAY
	        group by h.created,u.country
EOT;
        $query = $this->getReadConnection()->query($sql, [$days]);
        $query->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
        $data = $query->fetchAll();

        return $data;
    }

    /**
     * before validation on create
     */
    public function beforeValidationOnCreate()
    {
        $this->total_balance = 0;
        $this->bonus_balance = 0;
    }

    /**
     * validation
     */
    public function validation()
    {
        $validation = new Validation();

        // gender validation
        $validation->add(['total_balance', 'bonus_balance'], new Validation\Validator\Numericality());

        return $this->validate($validation);
    }
}
