<?php

namespace Knipster\Api\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;

/**
 * Class User
 *
 * @package Knipster\Api\Models
 */
class User extends Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $gender;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $email;

    /**
     * @var int
     */
    public $bonus;

    public function initialize()
    {
        $this->skipAttributesOnUpdate(['bonus']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user';
    }

    /**
     * @param array $parameters
     *
     * @return User[]
     */
    public static function find($parameters = [])
    {
        return parent::find($parameters);
    }

    /**
     * @param null $parameters
     *
     * @return User
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * validation
     */
    public function validation()
    {
        $validation = new Validation();

        // gender validation
        $validation->add('gender', new Validation\Validator\InclusionIn([
            'domain' => ['G', 'M']
        ]));

        // country validation
        $validation->add('country', new Validation\Validator\Regex([
            'pattern' => '/^[A-Z]{2}$/',
            'message' => 'Country must be in XX format'
        ]));

        // email validation
        $validation->add('email', new Validation\Validator\Email([
            'message' => 'E-mail is invalid'
        ]));

        return $this->validate($validation);
    }

    /**
     * before validation on create
     */
    public function beforeValidationOnCreate()
    {
        if (!$this->id) {
            $this->bonus = rand(5, 20);
        }
    }
}
