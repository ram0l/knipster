<?php

namespace Knipster\Api\Models;

use Lib\Api\ApiException;
use Phalcon\Mvc\Model;
use Phalcon\Validation;

/**
 * Class WalletHistory
 *
 * @package Knipster\Api\Models
 */
class WalletHistory extends Model
{
    const OPERATION_DEPOSIT = 1;
    const OPERATION_WITHDRAW = 2;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var int
     */
    public $wallet_id;

    /**
     * @var int
     */
    public $amount;

    /**
     * @var int
     */
    public $operation;

    /**
     * @var string
     */
    public $created;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'wallet_history';
    }

    public function initialize()
    {

    }

    /**
     * @param array $parameters
     *
     * @return Wallet[]
     */
    public static function find($parameters = [])
    {
        return parent::find($parameters);
    }

    /**
     * @param null $parameters
     *
     * @return Wallet
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @throws \Lib\Api\ApiException
     */
    public function beforeValidationOnUpdate()
    {
        throw new ApiException('Update of a wallet history not allowed');
    }

    /**
     * before validation on create
     */
    public function beforeValidationOnCreate()
    {
        $this->created = date('Y-m-d');
    }

    /**
     * validation
     */
    public function validation()
    {
        $validation = new Validation();

        // gender validation
        $validation->add('amount', new Validation\Validator\Numericality());

        // operation validation
        $validation->add('operation', new Validation\Validator\InclusionIn([
            'domain' => [
                self::OPERATION_DEPOSIT,
                self::OPERATION_WITHDRAW
            ]
        ]));

        return $this->validate($validation);
    }
}
