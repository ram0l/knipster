<?php

return new \Phalcon\Config([
    'application' => [
        'controllersDir' => __DIR__ . '/../controllers/',
        'modelsDir'      => __DIR__ . '/../models/',
        'migrationsDir'  => __DIR__ . '/../migrations/',
        'viewsDir'       => __DIR__ . '/../views/',
        'cacheDir'       => APP_PATH . '/cache/',
        'baseUri'        => '/'
    ],
    'security' => [
        'jwt_key' => 'LwTNbE9HLkfJehXg9H',
        'jwt_algorithm' => 'HS256'
    ]
]);
