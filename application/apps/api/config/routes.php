<?php
/**
 * @var \Phalcon\Mvc\Router $router
 */
// /Api/v1
$namespace = '\\Knipster\\Api\\Controllers';
$router->addPost('/api/user', [
    'namespace' => $namespace,
    'module' => 'Api',
    'controller' => 'user',
    'action' => 'add',
    'params' => 1
]);

$router->addPost('/api/user/:int', [
    'namespace' => $namespace,
    'module' => 'Api',
    'controller' => 'user',
    'action' => 'update',
    'id' => 1
]);

$router->addPost('/api/wallet/deposit', [
    'namespace' => $namespace,
    'module' => 'Api',
    'controller' => 'wallet',
    'action' => 'deposit'
]);

$router->addPost('/api/wallet/withdraw', [
    'namespace' => $namespace,
    'module' => 'Api',
    'controller' => 'wallet',
    'action' => 'withdraw'
]);

$router->addGet('/api/wallet/report', [
    'namespace' => $namespace,
    'module' => 'Api',
    'controller' => 'wallet',
    'action' => 'report'
]);
