<?php

namespace Knipster\Api\Controllers;

use Firebase\JWT\JWT;
use Lib\Api\Response;
use Phalcon\Http\Request;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;

/**
 * Class ControllerBase
 *
 * @package Knipster\Api\Controllers
 */
class ControllerBase extends Controller
{
    /**
     * @var Response
     */
    public $apiResponse;

    /**
     * initialize controller
     */
    public function initialize()
    {
        $this->apiResponse = new Response();
    }

    /**
     * This method returns proper json object for API calls
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function sendApiResponse(): ResponseInterface
    {
        $flag = isProd() ? null : JSON_PRETTY_PRINT;
        if (isProd()) {
            /*
             * this should be on production only
             */
            $this->response->setContentType('application/vnd.api+json', 'UTF-8');
        } else {
            /*
             * this can be displayed in browser so it's only needed for DEV mode
             */
            $this->response->setContentType('application/json', 'UTF-8');
        }

        if ($this->apiResponse->hasErrors()) {
            $this->response->setStatusCode(400, 'Bad request');
        }

        //$this->response->setContent(json_encode($this->apiResponse, $flag));
        $this->response->setJsonContent($this->apiResponse);
        return $this->response;
    }

    /**
     * check if call is authenticated
     */
    public function isAuth()
    {
        /**
         * @var Request $request
         */
        $request = $this->di->get('request');
        $authorizationHeader = $request->getHeader('Authorization');

        /*
         * jwt magic
         */
        $security = $this->di->get('config')->security;
        preg_match('/Bearer (.+)/', $authorizationHeader, $match);

        if (!isset($match[1])) {
            return false;
        }

        $encodedData = $match[1];

        try {
            JWT::decode($encodedData, $security->jwt_key, [$security->jwt_algorithm]);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * no access
     *
     * @return bool
     */
    public function noAccess(): bool
    {
        $this->dispatcher->forward([
            'controller' => 'errors',
            'action' => 'forbidden'
        ]);

        return true;
    }

    /**
     * method not allowed
     *
     * @return bool
     */
    public function methodNotAllowed(): bool
    {
        $this->dispatcher->forward(['controller' => 'errors', 'action' => 'methodNotAllowed']);
        return true;
    }
}
