<?php

namespace Knipster\Api\Controllers;

/**
 * Class IndexController
 *
 * @package Knipster\Api\Controllers
 */
class IndexController extends ControllerBase
{
    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        $this->apiResponse->setData(true);
        return $this->sendApiResponse();
    }
}
