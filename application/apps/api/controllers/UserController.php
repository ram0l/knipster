<?php

namespace Knipster\Api\Controllers;

use Knipster\Api\Models\User;
use Lib\Api\Error;

/**
 * Class UserController
 *
 * @package Knipster\Api\Controllers
 */
class UserController extends ControllerBase
{
    /**
     * POST /api/user
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function addAction()
    {
        $data = $this->request->getJsonRawBody();
        $model = new User();
        $model->assign(get_object_vars($data));
        $model->id = null;

        if (!$model->create()) {
            foreach ($model->getMessages() as $message) {
                $this->apiResponse->setError(new Error($message->getMessage()));
            }

            return $this->sendApiResponse();
        }

        $this->apiResponse->setData($model->toArray());
        return $this->sendApiResponse();
    }

    /**
     * POST /api/user/{id}
     *
     * @param int $id
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function updateAction(int $id)
    {
        $data = $this->request->getJsonRawBody();

        $model = User::findFirst($id);
        if (!$model) {
            $this->apiResponse->setError(new Error('User not found'));
            return $this->sendApiResponse();
        }

        $model->assign(get_object_vars($data));
        $model->id = $id;

        if (!$model->update()) {
            foreach ($model->getMessages() as $message) {
                $this->apiResponse->setError(new Error($message->getMessage()));
            }

            return $this->sendApiResponse();
        }

        $this->apiResponse->setData($model->refresh()->toArray());
        return $this->sendApiResponse();
    }
}
