<?php

namespace Knipster\Api\Controllers;

use Knipster\Api\Models\Wallet;
use Lib\Api\Error;

/**
 * Class WalletController
 *
 * @package Knipster\Api\Controllers
 */
class WalletController extends ControllerBase
{
    /**
     * POST /api/wallet/deposit
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function depositAction()
    {
        $data = $this->request->getJsonRawBody();

        if (!isset($data->user_id)) {
            $this->apiResponse->setError(new Error('user_id is required'));
            return $this->sendApiResponse();
        }

        if (!isset($data->amount)) {
            $this->apiResponse->setError(new Error('amount is required'));
            return $this->sendApiResponse();
        }

        // create wallet with lock for update
        $model = Wallet::factory($data->user_id);
        if (!$model) {
            $this->apiResponse->setError(new Error('User doesn\'t exist'));
            return $this->sendApiResponse();
        }

        // start transaction
        $this->db->begin();
        if (!$model->deposit($data->amount)) {
            foreach ($model->getMessages() as $message) {
                $this->apiResponse->setError(new Error($message->getMessage()));
            }

            // error, rollback
            $this->db->rollback();
            return $this->sendApiResponse();
        }
        $this->db->commit();

        $this->apiResponse->setData($model->toArray());
        return $this->sendApiResponse();
    }

    /**
     * POST /api/wallet/withdraw
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function withdrawAction()
    {
        $data = $this->request->getJsonRawBody();

        if (!isset($data->user_id)) {
            $this->apiResponse->setError(new Error('user_id is required'));
            return $this->sendApiResponse();
        }

        if (!isset($data->amount)) {
            $this->apiResponse->setError(new Error('amount is required'));
            return $this->sendApiResponse();
        }

        // create wallet with lock for update
        $model = Wallet::factory($data->user_id);
        if (!$model) {
            $this->apiResponse->setError(new Error('User doesn\'t exist'));
            return $this->sendApiResponse();
        }

        // start transaction
        $this->db->begin();
        if (!$model->withdraw($data->amount)) {
            foreach ($model->getMessages() as $message) {
                $this->apiResponse->setError(new Error($message->getMessage()));
            }

            // error, rollback
            $this->db->rollback();
            return $this->sendApiResponse();
        }
        $this->db->commit();

        $this->apiResponse->setData($model->toArray());
        return $this->sendApiResponse();
    }

    /**
     * GET /api/wallet/report
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function reportAction()
    {
        $days = $this->request->get('days', null, 7);

        $wallet = new Wallet();
        $data = $wallet->getReport((int)$days);

        $this->apiResponse->setData($data);
        return $this->sendApiResponse();
    }
}
