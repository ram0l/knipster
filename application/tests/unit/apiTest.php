<?php

/**
 * Class apiTest
 */
class apiTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testAddUser()
    {
        // create our http client (Guzzle)
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'http://localhost', array(
                'request.options' => array(
                    'exceptions' => false,
                )
            )]);

        $data = [
            'first_name' => 'FN_' . rand(0, 999),
            'last_name' => 'FN_' . rand(0, 999),
            'country' => 'MT',
            'gender' => 'M',
            'email' => 'e' . rand(0, 999) . '@test.com.mt'
        ];
        $response = $client->post('/api/user', ['json' => $data]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('first_name', $data['data']);
        $this->assertArrayHasKey('id', $data['data']);
    }
}
