<?php

namespace Lib\Core;

use Phalcon\Di\FactoryDefault;
use Phalcon\Version;

/**
 * Class Bootstrap
 *
 * @package Lib\Core
 */
class Bootstrap
{
    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * Bootstrap constructor.
     *
     * @param FactoryDefault $di
     */
    public function __construct(FactoryDefault $di)
    {
        $this->di = $di;
    }

    /**
     *  load config
     */
    public function loadConfig()
    {
        /**
         * Read the configuration
         *
         * @var \Phalcon\Config $config
         */
        $config = include APP_PATH . '/config/config.php';
        $this->di->setShared('config', function () use ($config) {
            return $config;
        });
    }

    /**
     * load services
     */
    public function loadServices()
    {
        $globalServices = [
            'router',
            'url',
            'db',
            'session',
            'metadata',
            'flash',
            'dispatcher',
            'cache'
        ];

        foreach ($globalServices as $service) {
            $class = '\Lib\Core\Service\\' . ucfirst($service);

            if (!class_exists($class)) {
                throw new CoreException('Service ' . ucfirst($service) . ' not found');
            }

            if (!method_exists($class, 'factory')) {
                throw new CoreException('Factory of service ' . ucfirst($service) . ' not found');
            }
            call_user_func($class . '::factory', $this->di);
        }
    }

    /**
     * Check environment configuration
     * Put it into cache if needed
     *
     * @return bool
     * @throws \Exception
     */
    public function checkEnvironment()
    {
        // check php version
        if (substr(phpversion(), 0, 1) < 7) {
            throw new \Exception('Required php version is lower than 7.x');
        }

        // check phalcon version
        if (Version::getPart(Version::VERSION_MAJOR) < 2) {
            throw new \Exception('Required phalcon version is lower than 2.x');
        }

        return true;
    }
}
