<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;

/**
 * Class Router
 *
 * @package Lib\Core\Service
 */
class Router
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Registering a router
         */
        $di->setShared('router', function () {
            $router = new \Phalcon\Mvc\Router();

            $router->setDefaultModule('api');
            $router->setDefaultNamespace('Knipster\Frontend\Controllers');

            return $router;
        });
    }
}
