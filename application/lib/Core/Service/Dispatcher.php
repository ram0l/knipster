<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;

/**
 * Class Dispatcher
 *
 * @package Lib\Core\Service
 */
class Dispatcher
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Set the default namespace for dispatcher
         */
        $di->setShared('dispatcher', function () use ($di) {
            $eventsManager = new \Phalcon\Events\Manager();
            $eventsManager->attach('dispatch:beforeException', function ($event, $dispatcher, $exception) use ($di) {
                /**
                 * @var \Phalcon\Mvc\Dispatcher $dispatcher
                 * @var \Exception              $exception
                 * @var \Phalcon\Events\Event   $event
                 */
                if ($event->getType() == 'beforeException') {
                    switch ($exception->getCode()) {
                        // 404 error
                        case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                        case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                            $dispatcher->forward([
                                'controller' => 'errors',
                                'action' => 'notFound',
                                'module' => 'Api',
                                'namespace' => '\\Knipster\\Api\\Controllers'
                            ]);

                            return false;

                        default:
                            // 500 error
                            $dispatcher->forward([
                                //'namespace' => 'Knipster\\Frontend\\Controllers',
                                'controller' => 'errors',
                                'action' => 'exception',
                                'module' => 'Api',
                                'namespace' => '\\Knipster\\Api\\Controllers',
                                'params' => [
                                    'exception' => $exception
                                ]
                            ]);

                            return false;
                    }
                }

                return true;
            });

            $dispatcher = new \Phalcon\Mvc\Dispatcher();
            $dispatcher->setDefaultNamespace('Knipster\Frontend\Controllers');
            $dispatcher->setEventsManager($eventsManager);
            return $dispatcher;
        });
    }
}
