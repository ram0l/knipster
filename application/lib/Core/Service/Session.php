<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * Class Session
 *
 * @package Lib\Core\Service
 */
class Session
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Starts the session the first time some component requests the session service
         */
        $di->setShared('session', function () {
            $session = new SessionAdapter();
            $session->start();

            return $session;
        });
    }
}
