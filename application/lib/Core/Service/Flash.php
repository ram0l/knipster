<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;
use Phalcon\Flash\Session as FlashMessages;

/**
 * Class Flash
 *
 * @package Lib\Core\Service
 */
class Flash
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Register the session flash service with the Twitter Bootstrap classes
         */
        $di->set('flash', function () {
            return new FlashMessages(array(
                'error' => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
                'warning' => 'alert alert-warning'
            ));
        });
    }
}
