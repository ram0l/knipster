<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;

/**
 * Class Db
 *
 * @package Lib\Core\Service
 */
class Db
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di->setShared('db', function () use ($di) {
            $config = $di->get('config');
            $dbConfig = $config->database->toArray();
            $adapter = $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            return new $class($dbConfig);
        });
    }
}
