<?php

namespace Lib\Core\Service;

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;

/**
 * Class Metadata
 *
 * @package Lib\Core\Service
 */
class Metadata
{
    /**
     * @param FactoryDefault $di
     */
    public static function factory(FactoryDefault $di)
    {
        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         */
        $di->setShared('modelsMetadata', function () {
            return new MetaDataAdapter();
        });
    }
}
