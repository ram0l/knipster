# Knipster test task

Task is done using [Phalcon Framework 3.x](http://phalconphp.com) and [Docker](http://docker.com)

## How to run using Docker

1. download and install Docker
2. `$ git clone git@gitlab.com:ram0l/knipster.git`
3. `$ docker-compose up --build`
4. add `127.0.0.1 phalcon.local` to /etc/hosts
5. play with postman ;)

## How to run on Linux
1. install [Phalcon](https://phalconphp.com/en/download)
2. prepare vhost according to [docs](https://docs.phalconphp.com/en/latest/index.html#installation)
3. project itself can be found here: `application/` which means Nginx should point into `application/public/index.php`

## Postman
Below prepared postman requests

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3ccf3353811e42fad177)

### Contact
Skype: ram0l